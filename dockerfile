# Start from Node 20 Alpine base image
FROM node:20-alpine


# Set environment variables for Puppeteer and Chrome
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
ENV CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/

# Update and install packages
RUN apk update && apk add --no-cache \
    chromium-swiftshader \
    ttf-freefont \
    font-noto-emoji \
    git \
    tini \
    && apk add --no-cache \
    --repository=https://dl-cdn.alpinelinux.org/alpine/edge/testing \
    font-wqy-zenhei

# Add Chrome user
RUN mkdir -p /usr/src/app \
    && adduser -D chrome \
    && chown -R chrome:chrome /usr/src/app

# Run Chrome as non-privileged
USER chrome
WORKDIR /usr/src/app

# Copy application code to container (separated into package files and everything else for caching purposes)
COPY package*.json ./
COPY . .

# Set entrypoint to be the Tini init system and Chromium browser
ENTRYPOINT [ "/sbin/tini", "--" ]
CMD [ "chromium-browser", "--headless", "--use-gl=swiftshader", "--disable-software-rasterizer", "--disable-dev-shm-usage", "--no-sandbox", "--hide-scrollbars", "--remote-debugging-port=9222", "--remote-debugging-address=0.0.0.0"]